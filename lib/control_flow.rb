# EASY

# Return the argument with all its lowercase characters removed.
def destructive_uppercase(str)
  str.each_char { |chr| str.delete!(chr) if chr == chr.downcase }
end

# Return the middle character of a string. Return the middle two characters if
# the word is of even length, e.g. middle_substring("middle") => "dd",
# middle_substring("mid") => "i"
def middle_substring(str)
  middle = str.length/2
  return str[middle] if str.length.odd?
  return str[middle-1] + str[middle] if str.length.even?
end

# Return the number of vowels in a string.
VOWELS = %w(a e i o u)
def num_vowels(str)
  str.count(VOWELS.join)
end

# Return the factoral of the argument (num). A number's factorial is the product
# of all whole numbers between 1 and the number itself. Assume the argument will
# be > 0.
def factorial(num)
  (1..num).inject { |factorial_of_num, next_num| factorial_of_num *= next_num }
end


# MEDIUM

# Write your own version of the join method. separator = "" ensures that the
# default seperator is an empty string.
def my_join(arr, separator = "")
  str = arr[0] || ""
  i = 1
  while i < arr.length
    str << separator << arr[i]
    i += 1
  end
  str
end

# Write a method that converts its argument to weirdcase, where every odd
# character is lowercase and every even is uppercase, e.g.
# weirdcase("weirdcase") => "wEiRdCaSe"
def weirdcase(str)
  weird_str = ""
  str.split("").each_index { |i| weird_str += i.even? ? str[i].downcase : str[i].upcase }
  weird_str
end

# Reverse all words of five or more letters in a string. Return the resulting
# string, e.g., reverse_five("Looks like my luck has reversed") => "skooL like
# my luck has desrever")
def reverse_five(str)
  reversed = []
  str.split.each { |word| reversed << ((word.length >= 5) ? word.reverse : word) }
  reversed.join(" ")
end

# Return an array of integers from 1 to n (inclusive), except for each multiple
# of 3 replace the integer with "fizz", for each multiple of 5 replace the
# integer with "buzz", and for each multiple of both 3 and 5, replace the
# integer with "fizzbuzz".
def fizzbuzz(n)
  (1..n).inject([]) do |arr, num|
    if num % 3 == 0 && num % 5 == 0
      arr << "fizzbuzz"
    elsif num % 3 == 0
      arr << "fizz"
    elsif num % 5 == 0
      arr << "buzz"
    else
      arr << num
    end
  end
end


# HARD

# Write a method that returns a new array containing all the elements of the
# original array in reverse order.
def my_reverse(arr)
  reversed = []
  i = arr.length-1
  while i >= 0
    reversed << arr[i]
    i -= 1
  end
  reversed
end

# Write a method that returns a boolean indicating whether the argument is
# prime.
def prime?(num)
  return false if num == 1
  (2...num).each { |number| return false if num % number == 0 }
  true
end

# Write a method that returns a sorted array of the factors of its argument.
def factors(num)
  (1..num).inject([]) { |arr, number| num % number == 0 ? arr << number : arr }
end

# Write a method that returns a sorted array of the prime factors of its argument.
def prime_factors(num)
  (1..num).inject([]) do |arr, number|
    if num % number == 0 && prime?(number)
      arr << number
    else
      arr
    end
  end
end

# Write a method that returns the number of prime factors of its argument.
def num_prime_factors(num)
  prime_factors(num).length
end


# EXPERT

# Return the one integer in an array that is even or odd while the rest are of
# opposite parity, e.g. oddball([1,2,3]) => 2, oddball([2,4,5,6] => 5)
def oddball(arr)
  sum = arr.inject(0) { |accum, num| accum + num }
  if sum.even?
    arr.each { |number| return number if number.even? }
  else
    arr.each { |number| return number if number.odd? }
  end
end
